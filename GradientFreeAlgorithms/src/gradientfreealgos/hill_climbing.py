import math

class SearchProblem:

    """
    An interface to define search problems.
    The interface will be a mathematical function.
    """

    def __init__(self, x: int, y: int, step_size: int, function_to_optimise):
        """
        The constructor for a search problem.
        x: the x coordinate of the current search state.
        y: the y coordinate of the current search state.
        step_size: the step size to take when looking for neighbours.
        function_to_optimise: a function to optimise with the signature: 
        f(x, y)
        """
        self.x = x
        self.y = y
        self.step_size = step_size
        self.function = function_to_optimise

    def score(self) -> int:
        """
        Returns the output of the function called with current x and y
        coordinates.
        >>> def test_function(x, y):
        ...     return x + y
        >>> SearchProblem(0, 0, 1, test_function).score() # 0 + 0 = 0
        0
        >>> SearchProblem(5, 7, 1, test_function).score() # 5 + 7 = 12  
        12
        """
        return self.function(self.x, self.y)

    def get_neighbours(self):
        """
        Returns a list of coordinates of neighbours adjacent to the current
        coordinates.

        Neighbours:
        | 0 | 1 | 2 |
        | 3 | _ | 4 |
        | 5 | 6 | 7 |
        """

        step_size = self.step_size
        return [
                SearchProblem(x, y, step_size, self.function)
                for x, y in (
                    (self.x - step_size, self.y - step_size),
                    (self.x - step_size, self.y),
                    (self.x - step_size, self.y + step_size),
                    (self.x, self.y - step_size),
                    (self.x, self.y + step_size),
                    (self.x + step_size, self.y - step_size),
                    (self.x + step_size, self.y),
                    (self.x + step_size, self.y + step_size)
                    )
                ]
    

    def __hash__(self):

        """
        Hash the string representation of the current search state.
        """
        return hash(str(self))

    def __eq__(self, obj):
        
        """
        Check if the two objects are equal.
        """
        if isinstance(obj, SearchProblem):
            return hash(str(self)) == hash(str(obj))
        return False
    
    def __str__(self):
        """
        String representation of the current search state.
        >>> str(SearchProblem(0, 0, 1, None))
        'x: 0 y: 0'
        >>> str(SearchProblem(2, 5, 1, None))
        'x: 2 y: 5'
        """
        return f"x: {self.x} y: {self.y}"


def hill_climbing(
    search_prob,
    find_max: bool = True,
    max_x: float = math.inf,
    min_x: float = -math.inf,
    max_y: float = math.inf,
    min_y: float = -math.inf,
    visualisation: bool = False,
    max_iter: int = 10000,
    ) -> SearchProblem:
        
    """
    Implementation of the hill climbing problem.
    We start with a given state, find all its neighbours,
    move towards the neighbour which provides the max or min change. We
    keep doing this until we are at the state where we do not have any
    neighbours which can improve the solution.
        Args:
            search_prob: The search state at the start.

            find_max: If True, the algorithm should find the maximum else
            the minimum.

            max_x, max_y, min_x, min_y: The max and min bounds of x and y.
            
            visualisation: If True, a matplotlib graph is displayed.
            max_iter: number of times to run the iteraiton.

        Returns a search state having the maximum (or minimum) score.
    """
    current_state = search_prob 
    scores = []
    iterations = 0
    solution_found = False
    visited = set()

    while not solution_found and iterations < max_iter:
        visited.add(current_state)
        iterations += 1
        current_score = current_state.score()
        scores.append(current_score)
        neighbours = current_state.get_neighbours()
        max_change = -math.inf
        min_change = math.inf
        next_state = None # To hold the next best neighbour
        for neighbour in neighbours:
            if neighbour in visited:
                continue # Do not want to visit the same state again
            if (
                neighbour.x > max_x
                or neighbour.x < min_x
                or neighbour.y > max_y
                or neighbour.y < min_y
               ):
                continue # neighbour outside the bound

            change = neighbour.score() - current_score

            if find_max: # Finding max
                if change > max_change and change > 0:
                    max_change = change
                    next_state = neighbour
            else: # finding min
                if change < min_change and change < 0:
                    min_change = change
                    next_state = neighbour
        if next_state is not None:
            # We found at least one neighbour which improved the current state
            current_state = next_state
        else:
            # since we have no neighbour that improves the solution, we stop
            # the search
            solution_found = True

    if visualisation:
        from matplotlib import pyplot as plt
        
        plt.plot(range(iterations), scores)
        plt.xlabel("Iterations")
        plt.ylabel("Function values")
        plt.show()

    return current_state


if __name__ == "__main__":
    import doctest

    doctest.testmod()

    def test_f1(x, y):
        return (x * x) + (y * y)

    # Starting the problem with initial coordinates (3, 4)
    prob = SearchProblem(x=3, y=4, step_size=1, function_to_optimise=test_f1)
    local_min = hill_climbing(prob, find_max=False)
    print(
            "The minimum score for f(x, y) = x^2 + y^2 found via hill climbing: "
            f"{local_min.score()}"
            )
    # Starting the problem with initial coordinates (12, 47)
    prob = SearchProblem(x=3, y=4, step_size=1, function_to_optimise=test_f1)
    local_min = hill_climbing(prob, find_max=False)
    print(
            "The minimum score for f(x, y) = x^2 + y^2 found via hill climbing: "
            f"{local_min.score()}"
            )
   
    def test_f2(x, y):
        return (3 * x**2) - (6 * y)

    prob = SearchProblem(x=3, y=4, step_size=1, function_to_optimise=test_f2)
    local_min = hill_climbing(prob, find_max=True)
    print(
            "The minimum score for f(x, y) = 3x^2 - 6y found via hill climbing: "
            f"{local_min.score()}"
            )
